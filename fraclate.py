#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

import pdb
st = pdb.set_trace

def rotate(vec, ang):
    """ Simple rotation of a vector of 2d points """
    assert False, "TODO"

def accum(vec, reversion=0):
    """ Sum up a 1D vector with intermediate values as it goes 
    TODO: make more efficient!"""
    res = np.empty_like(vec)
    sum = 0
    for k, v in enumerate(vec):
        sum += v
        sum *= (1-reversion)
        res[k] = sum

    return res

def rand_curve(start_pt, end_pt=None, start_ang=45, deriv=2,
               rand_fn=np.random.normal, scaling=20, reversion=3,
               segments=100, length=10, **kwargs):
    """Draw a single curving line starting from start_pt and either:
    - ending at end_pt
    - starting along the angle start_ang (in degrees)

    with segments=segments, and a random walk in the 2nd deriv of the
    angles (by default) scaled by rand_scaling, using rand_fn(x) to
    generate an array of x random values. Total line is of length
    length.
    
    start_pt: 1x2 starting coordinate

    """

    segstep = length/segments
    vec = np.empty([segments, 2])
    vec[0,:] = start_pt

    rand_angs = rand_fn(0, 1, segments) * scaling/segments
    revs = reversion/segments
    for k in range(deriv):
        rand_angs = accum(rand_angs, revs)

    # Make the initial angle agree
    # (TODO: optionally make start and endpoints match)
    rand_angs += start_ang - rand_angs[0]
    rand_angs *= np.pi/180
    
    prev_pt = start_pt
    for k in range(1, segments):
        step = np.array([np.cos(rand_angs[k]), np.sin(rand_angs[k])])*segstep
        new_pt = prev_pt + step
        prev_pt = new_pt
        vec[k] = prev_pt

    return vec

def add_branch(vec, idx, ang=90, **kwargs):
    """Each interv segments in vec, create a new curve starting from that
    segment but at 90 degrees from it."""

    vi = vec[idx]
    vip = vec[idx+1]

    local_ang = np.arctan2(vip[1]-vi[1], vip[0]-vi[0])*180/np.pi
    return rand_curve(vi, start_ang=local_ang+ang, **kwargs)

def plot_vec(vec, ax, **kwargs):
    ax.plot(vec[:,0], vec[:,1], linewidth=0.5, **kwargs)

def fraclate():
    stem = rand_curve([0,0], start_ang=90, segments=100)

    branches = []    
    for k in range(2,100,10):
        branches.append(add_branch(stem, k, 90, length=5 - 0.04*k, segments=20, scaling=50))

    for k in range(7,100,10):
        branches.append(add_branch(stem, k, -90, length=5 - 0.04*k, segments=20, scaling=50))


    twigs = []
    for b in branches:
        for k in range(2,20,4):
            twigs.append(add_branch(b, k, 90, length=1-0.03*k, segments=10,
                                    scaling=50, reversion=1))

        for k in range(4,20,4):
            twigs.append(add_branch(b, k, -90, length=1-0.03*k, segments=10,
                                    scaling=50, reversion=1))
            
    # Plotting
    fig, ax = plt.subplots(1, 1, figsize=(5, 5), dpi=72*2)        
    plot_vec(stem, ax)
    for b in branches:
        plot_vec(b, ax)

    for t in twigs:
        plot_vec(t, ax)
    
    ax.set_xlim([-5, 5])
    ax.set_ylim([0, 10])

    plt.show()
